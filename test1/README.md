实验1：SQL语句的执行计划分析与优化指导
2020104144329 周奥泽

实验目的
分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

实验数据库和用户
数据库是pdborcl，用户是sys和hr

实验内容

对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。


查询

我的查询语句：查询员工表中薪资最高和最低的两位员工是谁，然后根据部门id与工作id，以及location_id等信息查询他们所在的国家，并输出他们的部门名称与工作名称，两个查询的结果是一样的。但效率不相同。


查询1：


$sqlplus hr/123@localhost/pdborcl

set autotrace on
SELECT 
  e.employee_id,
  e.first_name || ' ' || e.last_name AS employee_name,
  e.salary,
  d.department_name,
  j.job_title,
  l.country_id
FROM 
  employees e
  JOIN departments d ON e.department_id = d.department_id
  JOIN jobs j ON e.job_id = j.job_id
  JOIN locations l ON d.location_id = l.location_id
WHERE 
  e.salary = (SELECT MAX(salary) FROM employees)
  OR e.salary = (SELECT MIN(salary) FROM employees);




代码分析
这段代码是用于查询员工表中薪水最高或最低的员工信息，其中使用了多个表的JOIN操作，包括employees、departments、jobs和locations表。
查询语句首先选择需要的列包括：员工ID（employee_id）、员工姓名（employee_name）、薪水（salary）、部门名称（department_name）、职位（job_title）和所在国家ID（country_id）。
接着，使用JOIN操作连接所有必要的表（employees、departments、jobs、locations），从而获取相关的信息。其中，使用ON子句指定JOIN的条件。这些条件分别使用部门ID、职位ID和位置ID对应表格中的对应列。
最后，在WHERE子句中，使用子查询（使用MAX和MIN聚合函数）获取薪水最高和最低的员工，然后将它们与员工表中的所有记录进行比较，如果薪水等于最高或最低薪水，就将该员工的所有列返回。

运行信息

EMPLOYEE_ID EMPLOYEE_NAME				       SALARY
----------- ---------------------------------------------- ----------
DEPARTMENT_NAME 	       JOB_TITLE			   CO
------------------------------ ----------------------------------- --
  132 TJ Olson						 2100
Shipping		       Stock Clerk			   US

  100 Steven King 					24000
Executive		       President			   US



执行计划
----------------------------------------------------------
Plan hash value: 2679607891

--------------------------------------------------------------------------------
--------------

| Id  | Operation		       | Name	     | Rows  | Bytes | Cost (%CP
U)| Time     |

--------------------------------------------------------------------------------
--------------

|   0 | SELECT STATEMENT	       |	     |	   4 |	 348 |	  12   (
9)| 00:00:01 |

|*  1 |  HASH JOIN		       |	     |	   4 |	 348 |	  12   (
9)| 00:00:01 |

|*  2 |   HASH JOIN		       |	     |	   4 |	 324 |	   9  (1
2)| 00:00:01 |

|   3 |    MERGE JOIN		       |	     |	   4 |	 248 |	   6  (1
7)| 00:00:01 |

|   4 |     TABLE ACCESS BY INDEX ROWID| JOBS	     |	  19 |	 513 |	   2   (
0)| 00:00:01 |

|   5 |      INDEX FULL SCAN	       | JOB_ID_PK   |	  19 |	     |	   1   (
0)| 00:00:01 |

|*  6 |     SORT JOIN		       |	     |	   4 |	 140 |	   4  (2
5)| 00:00:01 |

|*  7 |      TABLE ACCESS FULL	       | EMPLOYEES   |	   4 |	 140 |	   3   (
0)| 00:00:01 |

|   8 |       SORT AGGREGATE	       |	     |	   1 |	   4 |
  |	     |

|   9 |        TABLE ACCESS FULL       | EMPLOYEES   |	 107 |	 428 |	   3   (
0)| 00:00:01 |

|  10 |       SORT AGGREGATE	       |	     |	   1 |	   4 |
  |	     |

|  11 |        TABLE ACCESS FULL       | EMPLOYEES   |	 107 |	 428 |	   3   (
0)| 00:00:01 |

|  12 |    TABLE ACCESS FULL	       | DEPARTMENTS |	  27 |	 513 |	   3   (
0)| 00:00:01 |

|  13 |   TABLE ACCESS FULL	       | LOCATIONS   |	  23 |	 138 |	   3   (
0)| 00:00:01 |

--------------------------------------------------------------------------------
--------------


Predicate Information (identified by operation id):
---------------------------------------------------

  1 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
  2 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
  6 - access("E"."JOB_ID"="J"."JOB_ID")
      filter("E"."JOB_ID"="J"."JOB_ID")
  7 - filter("E"."SALARY"= (SELECT MAX("SALARY") FROM "EMPLOYEES" "EMPLOYEES")
OR

        "E"."SALARY"= (SELECT MIN("SALARY") FROM "EMPLOYEES" "EMPLOYEES"))

Note
-----
  - this is an adaptive plan


统计信息
----------------------------------------------------------
  30  recursive calls
    0  db block gets
  60  consistent gets
    0  physical reads
    0  redo size
      1046  bytes sent via SQL*Net to client
  608  bytes received via SQL*Net from client
    2  SQL*Net roundtrips to/from client
    1  sorts (memory)
    0  sorts (disk)
    2  rows processed



查询2：

$sqlplus hr/123@localhost/pdborcl

set autotrace on
SELECT 
  e.employee_id,
  e.first_name || ' ' || e.last_name AS employee_name,
  e.salary,
  d.department_name,
  j.job_title,
  l.country_id
FROM 
  (
    SELECT 
      employee_id,
      first_name,
      last_name,
      salary,
      department_id,
      job_id,
      ROW_NUMBER() OVER (ORDER BY salary DESC) AS max_salary_rank,
      ROW_NUMBER() OVER (ORDER BY salary ASC) AS min_salary_rank
    FROM 
      employees
  ) e
  JOIN departments d ON e.department_id = d.department_id
  JOIN jobs j ON e.job_id = j.job_id
  JOIN locations l ON d.location_id = l.location_id
WHERE 
  e.max_salary_rank = 1 OR e.min_salary_rank = 1;




代码分析
此查询语句是一个从employees表中选取数据的SELECT语句，融合了几个JOIN操作，目的是查找在工资最高和最低的员工信息。
在内部查询中使用了ROW_NUMBER()函数以计算每个雇员的工资排名并将其存储到临时表中。使用ORDER BY子句可以指定如何排序结果集。在此例中，ORDER BY salary DESC是按照工资从高到低排序，ORDER BY salary ASC是按照工资从低到高排序。
在外部查询中，使用了JOIN将包含员工信息的临时表e连接到departments, jobs和locations表。由于employee_id是employees表和临时表的共享列，因此可以在ON子句中指定e.department_id = d.department_id和e.job_id = j.job_id来连接这些表。

运行信息

EMPLOYEE_ID EMPLOYEE_NAME				       SALARY
----------- ---------------------------------------------- ----------
DEPARTMENT_NAME 	       JOB_TITLE			   CO
------------------------------ ----------------------------------- --
  132 TJ Olson						 2100
Shipping		       Stock Clerk			   US

  100 Steven King 					24000
Executive		       President			   US



执行计划
----------------------------------------------------------
Plan hash value: 3832319689

--------------------------------------------------------------------------------
-------------------

| Id  | Operation		       | Name		  | Rows  | Bytes | Cost
(%CPU)| Time	  |

--------------------------------------------------------------------------------
-------------------

|   0 | SELECT STATEMENT	       |		  |   106 | 15900 |    1
4  (22)| 00:00:01 |

|*  1 |  HASH JOIN		       |		  |   106 | 15900 |    1
4  (22)| 00:00:01 |

|   2 |   TABLE ACCESS FULL	       | JOBS		  |    19 |   513 |
3   (0)| 00:00:01 |

|*  3 |   HASH JOIN		       |		  |   106 | 13038 |    1
1  (28)| 00:00:01 |

|   4 |    MERGE JOIN		       |		  |    27 |   675 |
6  (17)| 00:00:01 |

|   5 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS	  |    27 |   513 |
2   (0)| 00:00:01 |

|   6 |      INDEX FULL SCAN	       | DEPT_LOCATION_IX |    27 |	  |
1   (0)| 00:00:01 |

|*  7 |     SORT JOIN		       |		  |    23 |   138 |
4  (25)| 00:00:01 |

|   8 |      TABLE ACCESS FULL	       | LOCATIONS	  |    23 |   138 |
3   (0)| 00:00:01 |

|*  9 |    VIEW 		       |		  |   107 | 10486 |
5  (40)| 00:00:01 |

|  10 |     WINDOW SORT 	       |		  |   107 |  3745 |
5  (40)| 00:00:01 |

|  11 |      WINDOW SORT	       |		  |   107 |  3745 |
5  (40)| 00:00:01 |

|  12 |       TABLE ACCESS FULL        | EMPLOYEES	  |   107 |  3745 |
3   (0)| 00:00:01 |

--------------------------------------------------------------------------------
-------------------


Predicate Information (identified by operation id):
---------------------------------------------------

  1 - access("E"."JOB_ID"="J"."JOB_ID")
  3 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
  7 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
      filter("D"."LOCATION_ID"="L"."LOCATION_ID")
  9 - filter("E"."MAX_SALARY_RANK"=1 OR "E"."MIN_SALARY_RANK"=1)


统计信息
----------------------------------------------------------
  15  recursive calls
    0  db block gets
  37  consistent gets
    0  physical reads
    0  redo size
      1046  bytes sent via SQL*Net to client
  608  bytes received via SQL*Net from client
    2  SQL*Net roundtrips to/from client
    3  sorts (memory)
    0  sorts (disk)
    2  rows processed



两种查询方式比较
这两段 SQL 查询语句都是用来找到工资最高和最低的员工，并列出他们的详细信息。然而，它们的实现方式略有不同。

第一段 SQL 查询语句中，使用了子查询（subquery）子句来找到最高和最低工资，然后在 WHERE 子句中应用这些条件。在 JOIN 子句中，通过将 employees、departments、jobs和locations 表联接在一起按需要显示的列进行显示。
第二段 SQL 查询语句中，先执行一个嵌套查询（nested query），通过在原始 employees 表的基础上增加两个计算字段 max_salary_rank, min_salary_rank , 在 JOIN 子句中，也是通过将 employees、departments、jobs和locations 表联接在一起按需要显示的列进行显示，然后在 WHERE 子句中使用排名（ranking）来找到最高和最低工资的员工。


对于两个 SQL 查询语句，使用情况可能有所不同：

如果是对于数据量比较小的表或者查询结果的行数比较小的情况，第一段 SQL 查询语句可以使用，因为通过简单的子查询，可以很容易的找到最高和最低工资的员工。
如果数据量比较大或者查询结果需要大量筛选，第二段 SQL 查询语句可能会更优，因为在嵌套查询之后再进行联结，可以使用索引等优化查询效率。



查询2的sql优化


建议：考虑接受推荐的sql概要文件


原理：此属性调整程序优化估计值


详细优化信息如下：

GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName95761
Tuning Task Owner  : HR
Tuning Task ID     : 41
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_101
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/20/2023 12:23:07
Completed at       : 03/20/2023 12:23:08

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : 0zm2q5r23nw4r
SQL Text      : SELECT 
                  e.employee_id,
                  e.first_name || ' ' || e.last_name AS employee_name,
                  e.salary,
                  d.department_name,
                  j.job_title,
                  l.country_id
                FROM 
                  (
                    SELECT 
                      employee_id,
                      first_name,
                      last_name,
                      salary,
                      department_id,
                      job_id,
                      ROW_NUMBER() OVER (ORDER BY salary DESC) AS
                max_salary_rank,
                      ROW_NUMBER() OVER (ORDER BY salary ASC) AS
                min_salary_rank
                    FROM 
                      employees
                  ) e
                  JOIN departments d ON e.department_id = d.department_id
                  JOIN jobs j ON e.job_id = j.job_id
                  JOIN locations l ON d.location_id = l.location_id
                WHERE 
                  e.max_salary_rank = 1 OR e.min_salary_rank = 1

-------------------------------------------------------------------------------
FINDINGS SECTION (1 finding)
-------------------------------------------------------------------------------

1- SQL Profile Finding (see explain plans section below)
--------------------------------------------------------
  为此语句找到了性能更好的执行计划。

  Recommendation (estimated benefit: 26.1%)
  -----------------------------------------
  - 考虑接受推荐的 SQL 概要文件。
    execute dbms_sqltune.accept_sql_profile(task_name => 'staName95761',
            task_owner => 'HR', replace => TRUE);

  Validation results
  ------------------
  已对 SQL profile 进行测试, 方法为执行其计划和原始计划并测量与计划相对应的执行统计信息。如果其中一个计划运行在很短的时间内就完成,
  则另一计划可能只执行了一部分。

                          Original Plan  With SQL Profile  % Improved
                          -------------  ----------------  ----------
  Completion Status:            COMPLETE          COMPLETE
  Elapsed Time (s):             .000395           .000213      46.07 %
  CPU Time (s):                 .000425           .000284      33.17 %
  User I/O Time (s):                  0                 0 
  Buffer Gets:                       23                17      26.08 %
  Physical Read Requests:             0                 0 
  Physical Write Requests:            0                 0 
  Physical Read Bytes:                0                 0 
  Physical Write Bytes:               0                 0 
  Rows Processed:                     2                 2 
  Fetches:                            2                 2 
  Executions:                         1                 1 

  Notes
  -----
  1. the original plan 的统计信息是 10 执行的平均值。
  2. the SQL profile plan 的统计信息是 10 执行的平均值。

-------------------------------------------------------------------------------
ADDITIONAL INFORMATION SECTION
-------------------------------------------------------------------------------
- 优化程序不能合并位于执行计划的行 ID 9 处的视图。. 优化程序不能合并包含窗口函数的视图。.

-------------------------------------------------------------------------------
EXPLAIN PLANS SECTION
-------------------------------------------------------------------------------

1- Original
-----------
Plan hash value: 3832319689


---------------------------------------------------------------------------------------------------
| Id  | Operation                      | Name             | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT               |                  |   106 | 15900 |    14  (22)| 00:00:01 |
|*  1 |  HASH JOIN                     |                  |   106 | 15900 |    14  (22)| 00:00:01 |
|   2 |   TABLE ACCESS FULL            | JOBS             |    19 |   513 |     3   (0)| 00:00:01 |
|*  3 |   HASH JOIN                    |                  |   106 | 13038 |    11  (28)| 00:00:01 |
|   4 |    MERGE JOIN                  |                  |    27 |   675 |     6  (17)| 00:00:01 |
|   5 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS      |    27 |   513 |     2   (0)| 00:00:01 |
|   6 |      INDEX FULL SCAN           | DEPT_LOCATION_IX |    27 |       |     1   (0)| 00:00:01 |
|*  7 |     SORT JOIN                  |                  |    23 |   138 |     4  (25)| 00:00:01 |
|   8 |      TABLE ACCESS FULL         | LOCATIONS        |    23 |   138 |     3   (0)| 00:00:01 |
|*  9 |    VIEW                        |                  |   107 | 10486 |     5  (40)| 00:00:01 |
|  10 |     WINDOW SORT                |                  |   107 |  3745 |     5  (40)| 00:00:01 |
|  11 |      WINDOW SORT               |                  |   107 |  3745 |     5  (40)| 00:00:01 |
|  12 |       TABLE ACCESS FULL        | EMPLOYEES        |   107 |  3745 |     3   (0)| 00:00:01 |
---------------------------------------------------------------------------------------------------

Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------

  1 - SEL$51A7896E
  2 - SEL$51A7896E / J@SEL$3
  5 - SEL$51A7896E / D@SEL$1
  6 - SEL$51A7896E / D@SEL$1
  8 - SEL$51A7896E / L@SEL$4
  9 - SEL$2        / E@SEL$1
  10 - SEL$2       
  12 - SEL$2        / EMPLOYEES@SEL$2

Predicate Information (identified by operation id):
---------------------------------------------------

  1 - access("E"."JOB_ID"="J"."JOB_ID")
  3 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
  7 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
      filter("D"."LOCATION_ID"="L"."LOCATION_ID")
  9 - filter("E"."MAX_SALARY_RANK"=1 OR "E"."MIN_SALARY_RANK"=1)

Column Projection Information (identified by operation id):
-----------------------------------------------------------

  1 - (#keys=1) "J"."JOB_TITLE"[VARCHAR2,35], "L"."COUNTRY_ID"[CHARACTER,2], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22]
  2 - (rowset=256) "J"."JOB_ID"[VARCHAR2,10], "J"."JOB_TITLE"[VARCHAR2,35]
  3 - (#keys=1; rowset=256) "L"."COUNTRY_ID"[CHARACTER,2], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "E"."JOB_ID"[VARCHAR2,10]
  4 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30], 
      "L"."COUNTRY_ID"[CHARACTER,2]
  5 - "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30], 
      "D"."LOCATION_ID"[NUMBER,22]
  6 - "D".ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]
  7 - (#keys=1) "L"."LOCATION_ID"[NUMBER,22], "L"."COUNTRY_ID"[CHARACTER,2]
  8 - "L"."LOCATION_ID"[NUMBER,22], "L"."COUNTRY_ID"[CHARACTER,2]
  9 - "E"."EMPLOYEE_ID"[NUMBER,22], "E"."FIRST_NAME"[VARCHAR2,20], 
      "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22], 
      "E"."JOB_ID"[VARCHAR2,10], "E"."MAX_SALARY_RANK"[NUMBER,22], 
      "E"."MIN_SALARY_RANK"[NUMBER,22]
  10 - (#keys=1) "SALARY"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY 
      INTERNAL_FUNCTION("SALARY") DESC )[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY "SALARY")[22]
  11 - (#keys=1) INTERNAL_FUNCTION("SALARY")[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY INTERNAL_FUNCTION("SALARY") DESC 
      )[22]
  12 - "EMPLOYEE_ID"[NUMBER,22], "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], 
      "JOB_ID"[VARCHAR2,10], "SALARY"[NUMBER,22], "DEPARTMENT_ID"[NUMBER,22]

2- Original With Adjusted Cost
------------------------------
Plan hash value: 3832319689


---------------------------------------------------------------------------------------------------
| Id  | Operation                      | Name             | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT               |                  |   106 | 15900 |    14  (22)| 00:00:01 |
|*  1 |  HASH JOIN                     |                  |   106 | 15900 |    14  (22)| 00:00:01 |
|   2 |   TABLE ACCESS FULL            | JOBS             |    19 |   513 |     3   (0)| 00:00:01 |
|*  3 |   HASH JOIN                    |                  |   106 | 13038 |    11  (28)| 00:00:01 |
|   4 |    MERGE JOIN                  |                  |    27 |   675 |     6  (17)| 00:00:01 |
|   5 |     TABLE ACCESS BY INDEX ROWID| DEPARTMENTS      |    27 |   513 |     2   (0)| 00:00:01 |
|   6 |      INDEX FULL SCAN           | DEPT_LOCATION_IX |    27 |       |     1   (0)| 00:00:01 |
|*  7 |     SORT JOIN                  |                  |    23 |   138 |     4  (25)| 00:00:01 |
|   8 |      TABLE ACCESS FULL         | LOCATIONS        |    23 |   138 |     3   (0)| 00:00:01 |
|*  9 |    VIEW                        |                  |   107 | 10486 |     5  (40)| 00:00:01 |
|  10 |     WINDOW SORT                |                  |   107 |  3745 |     5  (40)| 00:00:01 |
|  11 |      WINDOW SORT               |                  |   107 |  3745 |     5  (40)| 00:00:01 |
|  12 |       TABLE ACCESS FULL        | EMPLOYEES        |   107 |  3745 |     3   (0)| 00:00:01 |
---------------------------------------------------------------------------------------------------

Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------

  1 - SEL$51A7896E
  2 - SEL$51A7896E / J@SEL$3
  5 - SEL$51A7896E / D@SEL$1
  6 - SEL$51A7896E / D@SEL$1
  8 - SEL$51A7896E / L@SEL$4
  9 - SEL$2        / E@SEL$1
  10 - SEL$2       
  12 - SEL$2        / EMPLOYEES@SEL$2

Predicate Information (identified by operation id):
---------------------------------------------------

  1 - access("E"."JOB_ID"="J"."JOB_ID")
  3 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
  7 - access("D"."LOCATION_ID"="L"."LOCATION_ID")
      filter("D"."LOCATION_ID"="L"."LOCATION_ID")
  9 - filter("E"."MAX_SALARY_RANK"=1 OR "E"."MIN_SALARY_RANK"=1)

Column Projection Information (identified by operation id):
-----------------------------------------------------------

  1 - (#keys=1) "J"."JOB_TITLE"[VARCHAR2,35], "L"."COUNTRY_ID"[CHARACTER,2], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22]
  2 - (rowset=256) "J"."JOB_ID"[VARCHAR2,10], "J"."JOB_TITLE"[VARCHAR2,35]
  3 - (#keys=1; rowset=256) "L"."COUNTRY_ID"[CHARACTER,2], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "E"."JOB_ID"[VARCHAR2,10]
  4 - (#keys=0) "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30], 
      "L"."COUNTRY_ID"[CHARACTER,2]
  5 - "D"."DEPARTMENT_ID"[NUMBER,22], "D"."DEPARTMENT_NAME"[VARCHAR2,30], 
      "D"."LOCATION_ID"[NUMBER,22]
  6 - "D".ROWID[ROWID,10], "D"."LOCATION_ID"[NUMBER,22]
  7 - (#keys=1) "L"."LOCATION_ID"[NUMBER,22], "L"."COUNTRY_ID"[CHARACTER,2]
  8 - "L"."LOCATION_ID"[NUMBER,22], "L"."COUNTRY_ID"[CHARACTER,2]
  9 - "E"."EMPLOYEE_ID"[NUMBER,22], "E"."FIRST_NAME"[VARCHAR2,20], 
      "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22], 
      "E"."JOB_ID"[VARCHAR2,10], "E"."MAX_SALARY_RANK"[NUMBER,22], 
      "E"."MIN_SALARY_RANK"[NUMBER,22]
  10 - (#keys=1) "SALARY"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY 
      INTERNAL_FUNCTION("SALARY") DESC )[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY "SALARY")[22]
  11 - (#keys=1) INTERNAL_FUNCTION("SALARY")[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY INTERNAL_FUNCTION("SALARY") DESC 
      )[22]
  12 - "EMPLOYEE_ID"[NUMBER,22], "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], 
      "JOB_ID"[VARCHAR2,10], "SALARY"[NUMBER,22], "DEPARTMENT_ID"[NUMBER,22]

3- Using SQL Profile
--------------------
Plan hash value: 1562326085


-----------------------------------------------------------------------------------------------
| Id  | Operation                       | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                |             |     2 |   300 |    12  (25)| 00:00:01 |
|   1 |  NESTED LOOPS                   |             |     2 |   300 |    12  (25)| 00:00:01 |
|   2 |   NESTED LOOPS                  |             |     2 |   300 |    12  (25)| 00:00:01 |
|   3 |    NESTED LOOPS                 |             |     2 |   288 |    10  (30)| 00:00:01 |
|   4 |     MERGE JOIN                  |             |     2 |   250 |     8  (38)| 00:00:01 |
|   5 |      TABLE ACCESS BY INDEX ROWID| JOBS        |    19 |   513 |     2   (0)| 00:00:01 |
|   6 |       INDEX FULL SCAN           | JOB_ID_PK   |    19 |       |     1   (0)| 00:00:01 |
|*  7 |      SORT JOIN                  |             |   107 | 10486 |     6  (50)| 00:00:01 |
|*  8 |       VIEW                      |             |   107 | 10486 |     5  (40)| 00:00:01 |
|   9 |        WINDOW SORT              |             |   107 |  3745 |     5  (40)| 00:00:01 |
|  10 |         WINDOW SORT             |             |   107 |  3745 |     5  (40)| 00:00:01 |
|  11 |          TABLE ACCESS FULL      | EMPLOYEES   |   107 |  3745 |     3   (0)| 00:00:01 |
|  12 |     TABLE ACCESS BY INDEX ROWID | DEPARTMENTS |     1 |    19 |     1   (0)| 00:00:01 |
|* 13 |      INDEX UNIQUE SCAN          | DEPT_ID_PK  |     1 |       |     0   (0)| 00:00:01 |
|* 14 |    INDEX UNIQUE SCAN            | LOC_ID_PK   |     1 |       |     0   (0)| 00:00:01 |
|  15 |   TABLE ACCESS BY INDEX ROWID   | LOCATIONS   |     1 |     6 |     1   (0)| 00:00:01 |
-----------------------------------------------------------------------------------------------

Query Block Name / Object Alias (identified by operation id):
-------------------------------------------------------------

  1 - SEL$51A7896E
  5 - SEL$51A7896E / J@SEL$3
  6 - SEL$51A7896E / J@SEL$3
  8 - SEL$2        / E@SEL$1
  9 - SEL$2       
  11 - SEL$2        / EMPLOYEES@SEL$2
  12 - SEL$51A7896E / D@SEL$1
  13 - SEL$51A7896E / D@SEL$1
  14 - SEL$51A7896E / L@SEL$4
  15 - SEL$51A7896E / L@SEL$4

Predicate Information (identified by operation id):
---------------------------------------------------

  7 - access("E"."JOB_ID"="J"."JOB_ID")
      filter("E"."JOB_ID"="J"."JOB_ID")
  8 - filter("E"."MAX_SALARY_RANK"=1 OR "E"."MIN_SALARY_RANK"=1)
  13 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
  14 - access("D"."LOCATION_ID"="L"."LOCATION_ID")

Column Projection Information (identified by operation id):
-----------------------------------------------------------

  1 - (#keys=0) "J"."JOB_TITLE"[VARCHAR2,35], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "D"."LOCATION_ID"[NUMBER,22], 
      "L"."COUNTRY_ID"[CHARACTER,2]
  2 - (#keys=0) "J"."JOB_TITLE"[VARCHAR2,35], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "D"."LOCATION_ID"[NUMBER,22], "L".ROWID[ROWID,10]
  3 - (#keys=0) "J"."JOB_TITLE"[VARCHAR2,35], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "D"."DEPARTMENT_NAME"[VARCHAR2,30], "D"."LOCATION_ID"[NUMBER,22]
  4 - (#keys=0) "J"."JOB_TITLE"[VARCHAR2,35], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "E"."DEPARTMENT_ID"[NUMBER,22]
  5 - "J"."JOB_ID"[VARCHAR2,10], "J"."JOB_TITLE"[VARCHAR2,35]
  6 - "J".ROWID[ROWID,10], "J"."JOB_ID"[VARCHAR2,10]
  7 - (#keys=1) "E"."JOB_ID"[VARCHAR2,10], "E"."EMPLOYEE_ID"[NUMBER,22], 
      "E"."FIRST_NAME"[VARCHAR2,20], "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], 
      "E"."DEPARTMENT_ID"[NUMBER,22]
  8 - "E"."EMPLOYEE_ID"[NUMBER,22], "E"."FIRST_NAME"[VARCHAR2,20], 
      "E"."LAST_NAME"[VARCHAR2,25], "E"."SALARY"[NUMBER,22], "E"."DEPARTMENT_ID"[NUMBER,22], 
      "E"."JOB_ID"[VARCHAR2,10], "E"."MAX_SALARY_RANK"[NUMBER,22], 
      "E"."MIN_SALARY_RANK"[NUMBER,22]
  9 - (#keys=1) "SALARY"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY 
      INTERNAL_FUNCTION("SALARY") DESC )[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY "SALARY")[22]
  10 - (#keys=1) INTERNAL_FUNCTION("SALARY")[22], "EMPLOYEE_ID"[NUMBER,22], 
      "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], "JOB_ID"[VARCHAR2,10], 
      "DEPARTMENT_ID"[NUMBER,22], ROW_NUMBER() OVER ( ORDER BY INTERNAL_FUNCTION("SALARY") 
      DESC )[22]
  11 - "EMPLOYEE_ID"[NUMBER,22], "FIRST_NAME"[VARCHAR2,20], "LAST_NAME"[VARCHAR2,25], 
      "JOB_ID"[VARCHAR2,10], "SALARY"[NUMBER,22], "DEPARTMENT_ID"[NUMBER,22]
  12 - "D"."DEPARTMENT_NAME"[VARCHAR2,30], "D"."LOCATION_ID"[NUMBER,22]
  13 - "D".ROWID[ROWID,10]
  14 - "L".ROWID[ROWID,10]
  15 - "L"."COUNTRY_ID"[CHARACTER,2]

Note
-----
  - this is an adaptive plan

-------------------------------------------------------------------------------