# 实验4：PL/SQL语言打印杨辉三角

- **姓名：周奥泽** 
- **学号：202010414329**

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```
1. 首先在数据库pdbocrl_hr中创建一个新的过程，名为YHTriange
![](1.png)

2. 在YHTriange的过程中写入创建YHTriange的SQL语句

```sql
create or replace PROCEDURE YHTriangle(N IN integer)
AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
```
![](2.png)

3. 使用以下命令测试该存储过程，并打印出前10行杨辉三角：
```sql
SET SERVEROUTPUT ON;
BEGIN
YHTriangle(10);
END;
```
![](3.png)

## 实验总结

    本次实验主要是学习Oracle PL/SQL语言的基础知识，了解如何编写存储过程以及如何使用存储过程去打印杨辉三角。

    首先，我们学习了Oracle PL/SQL语言的基础语法，包括变量的定义、数组的使用、循环和条件语句等。

    接着，我们对源代码进行了分析，了解了杨辉三角的生成原理和方法，也对源代码进行了一些修改，使其能够转变为一个存储过程。

    在编写存储过程时，我们使用了CREATE OR REPLACE PROCEDURE语句来定义存储过程，使用了IN参数来接收行数N，使用了dbms_output.put_line来打印杨辉三角。

    最后，我们还学习了如何在hr用户下创建存储过程，并使用SQL语句来调用存储过程。

    总之，在本次实验中，我们通过实际操作掌握了Oracle PL/SQL语言的编写及存储过程的使用方法，并且熟悉了Oracle数据库中的dbms_output包的使用，可以方便实现输出。这些知识和技能对于日后的工作和学习都非常有帮助。
