# 实验2：用户及权限管理

姓名：周奥泽   学号：202010414329  班级：软件工程(3)班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 步骤1：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

```
$ sqlplus system/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
REVOKE con_res_role FROM sale;
```

运行结果：

![](./pic1.png)

- 步骤2：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```
SQL> show user;
USER 为 "SALE"
SQL> SELECT * FROM session_privs;

PRIVILEGE
----------------------------------------
CREATE SESSION
CREATE ANY TABLE

SQL> SELECT * FROM session_roles;

未选定行

SQL> CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;

表已创建。

SQL> INSERT INTO customers(id,name)VALUES(1,'zhang');

已创建 1 行。

SQL> INSERT INTO customers(id,name)VALUES (2,'wang');

已创建 1 行。


SQL> CREATE VIEW customers_view AS SELECT name FROM customers;

视图已创建。

SQL> GRANT SELECT ON customers_view TO hr;

授权成功。

SQL> SELECT * FROM customers_view;

NAME
--------------------------------------------------
zhang
wang

```

需注意先要给sale用户进行授权操作，使其能够创建表与视图以及查找操作

```
SQL> GRANT CREATE ANY TABLE TO sale;

授权成功。

SQL> GRANT CREATE ANY VIEW TO sale;

授权成功。

SQL> GRANT SELECT ANY TABLE TO sale;
```



- 步骤3：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers_view;

NAME
--------------------------------------------------
zhang
wang


```

运行结果：

![](./pic2.png)

sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```
$ sqlplus system/123@pdborcl
SQL> ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

配置文件已更改

```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```
SQL> alter user sale  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

### *查看数据库的使用情况*

查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```
$ sqlplus system/123@pdborcl
SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

TABLESPACE_NAME
------------------------------
FILE_NAME
--------------------------------------------------------------------------------
	MB     MAX_MB AUT
---------- ---------- ---
USERS
/home/oracle/app/oracle/oradata/orcl/pdborcl/users01.dbf
	 5 32767.9844 YES


SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
  7    8   where  a.tablespace_name = b.tablespace_name;

表空间名                           大小MB     剩余MB     使用MB    使用率%
------------------------------ ---------- ---------- ---------- ----------
SYSAUX				      370     23.375	346.625      93.68
UNDOTBS1			      100      69.25	  30.75      30.75
USERS					5     3.9375	 1.0625      21.25
SYSTEM				      260      9.125	250.875      96.49


```

运行结果：

![](./pic3.png)

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。

## 实验结束删除用户和角色

```
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```

运行结果：

![](./pic4.png)

## 实验总结

​	用户创建后不能直接登录数据库及对数据库进行操作，需要赋予用户一定的权限才可以进行相应的操作。
​	角色是一组相关权限的命名集合，使用角色最主要的目的是简化权限管理。将一组权限打包到角色中，赋权时可直接将角色赋予用户，代表将角色下得所有权限都赋予了用户，简化了赋权操作
​	权限分为两类
系统权限: 允许用户执行特定的数据库动作，如创建表、创建索引、连接实例等
对象权限: 允许用户操纵一些特定的对象，如读取视图，可更新某些列、执行存储过程等
​	通过这次实验，我了解到了Oracle中的一些有关用户操作的功能，例如用户管理、角色管理、权根维护与分配的能力，在实验过程中我还掌握了用户之间共享对象的操作技能。在实验完成后，能够独自完成一些简单的oracle数据库用户操作。
